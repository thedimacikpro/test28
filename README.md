## Инструкция

Скопируйте .env.example в .env и поставьте свой пароль для БД(DB_PASSWORD)

Далее выполните следующие команды: 

composer install --ignore-platform-reqs 

php artisan config:cache 

php artisan route:cache 

php artisan migrate 

php artisan db:seed 

php artisan serve


Далее можете проверять АПИ, через Postman или аналогичную программу
АПИ доступно по умолчанию: http://127.0.0.1:8000/api