<?php

use Illuminate\Support\Facades\Route;

Route::get('/brands',[\App\Http\Controllers\Api\CarController::class,'brands']);
Route::get('/models',[\App\Http\Controllers\Api\CarController::class,'models']);

Route::prefix('cars')->group(function () {
    Route::get('/',[\App\Http\Controllers\Api\CarController::class,'index']);
    Route::get('/get_my_cars',[\App\Http\Controllers\Api\CarController::class,'getMyCars']);
    Route::post('/',[\App\Http\Controllers\Api\CarController::class,'store']);
    Route::prefix('/{car}')->group(function () {
        Route::get('/',[\App\Http\Controllers\Api\CarController::class,'show']);
        Route::put('/',[\App\Http\Controllers\Api\CarController::class,'update']);
        Route::delete('/',[\App\Http\Controllers\Api\CarController::class,'delete']);
    });
});
