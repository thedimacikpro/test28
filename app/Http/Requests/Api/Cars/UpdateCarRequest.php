<?php

namespace App\Http\Requests\Api\Cars;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCarRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'year_of_issue' => 'nullable|integer|digits:4',
            'mileage'       => 'nullable|integer',
            'color'         => 'nullable|string',
            'car_model_id'  => 'required|exists:car_models,id',
        ];
    }

    public function messages()
    {
        return [
            'car_model_id.required' => 'Выберите модель автомобиля',
            'car_model_id.exists'   => 'Данной модели не существует',
            'year_of_issue.integer' => 'Год выпуска должен быть числом',
            'mileage.integer'       => 'Значение пробега должно быть числом',
            'color.string'          => 'Цвет должен иметь строковый тип данных',
            'year_of_issue.digits'  => 'Год выпуска должен состоять из 4 цифр',
        ];
    }
}
