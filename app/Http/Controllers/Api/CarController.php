<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Cars\StoreCarRequest;
use App\Http\Requests\Api\Cars\UpdateCarRequest;
use App\Resources\Api\CarResource;
use App\Models\Car;
use App\Models\CarBrand;
use App\Models\CarModel;

class CarController extends Controller
{
    public function models()
    {
        return CarModel::with('brand')->get();
    }

    public function brands()
    {
        return CarBrand::all();
    }

    public function index()
    {
        return CarResource::collection(Car::with(['model' => fn($m) => $m->with('brand')])->get());
    }

    public function getMyCars()
    {
        if ($user = auth()->user()) {
            return CarResource::collection($user->cars()?->with(['model' => fn($m) => $m->with('brand')])->get());
        }

        return [];
    }

    public function show(Car $car)
    {
        return CarResource::make($car->load(['model' => fn($m) => $m->with('brand')]));
    }

    public function store(StoreCarRequest $request)
    {
        $car = Car::create($request->safe()->toArray());

        return $this->show($car);
    }

    public function update(Car $car, UpdateCarRequest $request)
    {
        $car->update($request->safe()->toArray());

        return $this->show($car->refresh());
    }

    public function delete(Car $car)
    {
        return $car->delete();
    }
}
