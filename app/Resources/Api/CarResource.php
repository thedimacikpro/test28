<?php

namespace App\Resources\Api;

use App\Models\Car;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        /** @var Car $this*/
        return [
            'id'            => $this->id,
            'year_of_issue' => $this->year_of_issue,
            'user_id'       => $this->user_id,
            'mileage'       => $this->mileage,
            'color'         => $this->color,
            'car_model_id'  => $this->car_model_id,
            'model'         => $this->model,
        ];
    }
}
