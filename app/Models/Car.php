<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    public $guarded = [];

    public $timestamps = false;

    public function model()
    {
        return $this->belongsTo(CarModel::class,'car_model_id','id');
    }
}
