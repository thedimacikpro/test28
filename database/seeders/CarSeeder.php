<?php

namespace Database\Seeders;

use App\Models\Car;
use App\Models\CarModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        CarModel::query()->get()->map(function ($model) {
            Car::create([
                'car_model_id' => $model->id,
                'year_of_issue' => $model->id % 2 === 0 ? rand(2010, 2023) : null,
                'mileage' => $model->id % 2 === 0 ? rand(10000, 300000) : null,
                'color' => random_color()
            ]);
        });
    }
}
