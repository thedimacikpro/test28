<?php

namespace Database\Seeders;

use App\Models\CarBrand;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CarBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach (['Volkswagen','Toyota','Mercedes-Benz Group'] as $item) {
            CarBrand::create([
                'name' => $item
            ]);
        }
    }
}
