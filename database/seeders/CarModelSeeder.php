<?php

namespace Database\Seeders;

use App\Models\CarModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CarModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $volkswagen = ['Atlas','Atlas Cross Sport','Tiguan'];
        $toyota = ['Camry','Camry Hybrid','Corolla'];
        $mercedes = ['A-Class','A-Class Saloon','B-Class'];

        foreach ($volkswagen as $item) {
            CarModel::create([
                'name' => $item,
                'car_brand_id' => 1
            ]);
        }

        foreach ($toyota as $item) {
            CarModel::create([
                'name' => $item,
                'car_brand_id' => 2
            ]);
        }

        foreach ($mercedes as $item) {
            CarModel::create([
                'name' => $item,
                'car_brand_id' => 3
            ]);
        }
    }
}
